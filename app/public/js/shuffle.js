'use strict';

module.exports = {
  shuffleArrayElements, randomInt
};

//Function to generate random number
function randomInt (lo, hi) {
  return Math.floor(Math.random() * (hi - 1)) + lo;
}

//Function to shuffle array elements
function shuffleArrayElements(array) {
  let index, buffer;
  let shuffledArray = [];
  //Create a duplicate array
  for(let i = 0; i < array.length ;i++) {
    shuffledArray[i] = array[i];
  }
  //Shuffle the duplicate array
  for(let i = 0; i < array.length ;i++) {
    index = randomInt(0, array.length - 1);
    buffer = shuffledArray[i];
    shuffledArray[i] = shuffledArray[index];
    shuffledArray[index] = buffer;
  }
  return shuffledArray;
}
