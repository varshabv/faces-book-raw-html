'use strict';

const shuffleObject = require('./shuffle.js');
const assert = require('assert');

function testRandomInt(lo, hi) {
  let randomNumber;
  let testOccurance = [];
  for(let i = 0; i < (hi - lo); i ++) {
     testOccurance[i] = false;
  }
  for (let i = 0; i < lo * 1000; i ++) {
    randomNumber = shuffleObject.randomInt(lo, hi);
    testOccurance[randomNumber - lo] = true;
  }
  for(let i = 0; i < (hi - lo); i ++) {
     assert.equal(true, testOccurance[i]);
  }
}

function checkShuffling(array, shuffledArray) {
  let count = 0;
  for(let i = 0; i < array.length; i++) {
     if(array[i] === shuffledArray[i]) {
       count ++;
     }
     else {
       return 0;
     }
  }
  if(count === array.length) {
    return 1;
  }
}

function test_shuffling_of_array_elements(array) {
  let shuffle = shuffleObject.shuffleArrayElements(array);
  assert.equal(1, checkShuffling(array.sort(), shuffle.sort()));
}

testRandomInt(1,5);
testRandomInt(1000,2000);
test_shuffling_of_array_elements( [1,2,3,4,5,6]);
test_shuffling_of_array_elements( [9,5,17,1,11,15,25]);
test_shuffling_of_array_elements(["Harry","Hermione","Ron","Ginny","Neville"]);
test_shuffling_of_array_elements(["Harry.jpg","Hermione.jpg","Ron.jpg","Ginny.jpg","Neville.jpg"]);

console.log("All tests passed");
