$(document).ready(function() {

  // ready for Javascript which will run when the html is loaded

  const faces = $('faces');
  //const add = $('<b>').html('Hello');
  //faces.append(add);
  const makeFace = function (name) {
    return $('<img>', {
      class :"face",
      title : name,
      src:`img/2018/${name}.jpg`
    });
  };
  const names = [ "Akshat","Karthika","Sanjana", "Mahidher", "Akhil", "Keerthana", "RVishnuPriya", "Anushree", "Mariah", "Aishu", "Rishi", "Jon", "Pavithrann", "Sindhu", "Supriya", "Shruti", "Santhosh", "Supriya", "Gayatri", "Vishnu", "Varsha", "John", "Ameya", "Sudeep", "Janani", "Thiru", "Thamizh" ];
  names.forEach(function(name) {
   faces.append(makeFace(name));
 });
});
